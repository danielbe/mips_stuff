.data
AskForWeaponText:
	.asciiz "\Please enter your weapon or Q\n"
UserWonText:
	.asciiz "\You won\n"
ComputerWonText:
	.asciiz "\You lost\n"
NobodyWonText:
	.asciiz "\A draw\n"
insert_into:
	.space 3 #kann wohl auch an den anfang
			#problem mit backspace ua noch

# 00000a50 P
# 00000a51 Q
# 00000a52 R
# 00000a53 S

.text
main:
#t0 weapon of user
#t1 weapon of computer
#t3 case P
#t4 case Q
#t5 case R
#t6 case S

#initialize t3-t6 (easier to compare against later)
	addiu $t3, $0, 0x0a50	#P
	addiu $t4, $t3, 1	#Q
	addiu $t5, $t4, 1	#R
	addiu $t6, $t5, 1	#S

	start: #label we go back to after a game

#print text that asks for the weapon
	la $a0, AskForWeaponText
	addiu $v0, $0, 4	#syscall opcode for print string
	syscall			#opcode 4 with argument in a0

#store user input in insert_into and in $t0
	la $a0, insert_into	#allocated space
	#la $a1, insert_into	#max read characters
	addiu $a1, $0, 3
	addiu $v0, $0, 8	#syscall opcode for reading string
	syscall			#opcode 8 with arguments in a0 and a1
	lh $t0, insert_into	#we only load the first 2 bytes (1 character) of user input
				#lh load halfword only loads first 2 bytes

#check if user input was P, Q, R or S
	beq $t0, $t3, userInputCorrect
	nop
	beq $t0, $t4, end	#jump to end of program if user typed Q
	nop
	beq $t0, $t5, userInputCorrect
	nop
	beq $t0, $t6, userInputCorrect
	nop
	#if we reach this code, user input was incorrect
	j start
	nop

userInputCorrect:
#get random number 1, 2 or 3 (for computer weapon selection)
	addiu $v0, $0, 42
	addu $a0, $0, $0
	addiu $a1, $0, 3
	syscall			#opcode 42 (generate random integer > 0 and < $a1)
	addu $t1, $a0, $t3	#syscall result $a0 + value of P
	bne $t1, $t4, notQ	#computer result could be Q
	addiu $t1, $t1, 2	#in that case we add 2 so it will be S
	notQ:

#compare user and computer choice to find winner
	beq $t0, $t1, draw	#draw when user and computer have same weapon
	nop
	
	#user has paper?
	bne $t0, $t3, userNoPaper
	nop
	#yes
	beq $t1, $t5, userWon	#computer has rock and loses
	nop
	j computerWon		#computer must have scissors and wins
	nop
	
	userNoPaper:
	#user has rock?
	bne $t0, $t5, userNoRock
	nop
	#yes
	beq $t1, $t3, computerWon	#computer has paper and wins
	nop
	j userWon			#computer must have scissors and loses
	nop
	
	userNoRock:
	#user must have scissors
	beq $t1, $t3, userWon	#computer has paper and loses
	nop
	j computerWon		#computer must have rock and wins
	nop
	
#load winner text in $a0
	computerWon:
	la $a0, ComputerWonText
	j printWinner
	nop
	
	userWon:
	la $a0, UserWonText
	j printWinner
	nop
	
	draw:
	la $a0, NobodyWonText	#no jump needed

	printWinner:
#print text that displays winner
	addiu $v0, $0, 4	#syscall opcode for print string
	syscall			#opcode 4 with argument in a0

#start next game, user did not Quit
j start
nop

#end the program
end:	#if user typed Q we jump and land here
	addiu $v0, $0, 10 #loads op code into $v0 to exit program
	syscall #reads $v0 and exits program
	
	
	
	
	
	
