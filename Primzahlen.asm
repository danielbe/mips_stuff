#Zweistellige Primzahlen zu einer vorgegebenen Obergrenze berechnen

.data
welcome: .asciiz "\Geben Sie die Obergrenze an "

##########################
#### t register usage ####
##########################
# $t0 user input
# $t1 number counting up until user input (int i)
# $t2 number 2
# $t3 user input / t3 = prime?
# $t4 can prime check be skipped
# $t6 remainder of division for prime check
# $t7 number 7

.text
main:
#print introduction text
	la $a0, welcome
	addiu $v0, $0, 4
	syscall			#opcode 4 prints string in a0

#store user input in t0
	addiu $v0, $0, 5
	syscall			#opcode 5 reads integer into v0
	addu $t0, $0, $v0	#move value to t0

#prepare registers with variables for later
	addiu $t2, $0, 2
	addiu $t3, $0, 2
	addiu $t4, $0, 4
	addiu $t7, $0, 7
	
#for (int i = 1; ...)
	addiu $t1, $0, 11

#no above condition met: calculating
loop:

#check if dividable (t3 will equal 2, then 3, then 5, then 7)
	div $t1, $t3		#divide by 2, remainder is in hi
	mfhi $t6		#move hi value to t6
	beq $t6, $0, nextnumber	#remainder == 0 -> not prime
	nop
	
	beq $t3, $t7, print
	nop
	beq $t3, $t2, addone
	nop
	beq $t1, $t7, nextnumber
	nop
	addiu $t3, $t3, 2	#addtwo: 3 -> 5 or 5 -> 7
	j loop
	nop
	
addone:				#2 -> 3
	addiu $t3, $t3, 1
	j loop
	nop

print:
#prime - print the number
	addu $a0, $0, $t1
	jal printnumberina0
	nop
	
	j nextnumber
	nop

nextnumber:
	beq $t1, $t0, exit	#stop when we reach user input number
	nop
	addiu $t1, $t1, 1	#int i++
	addu $t3, $0, $t2	#reset t3 to 2, for prime next check
	j loop
	nop

printnumberina0:
#prime - print the number
	addiu $v0, $0, 1
	syscall			#opcode 1 prints integer in a0
#print space
	addiu $a0, $0, 0x0020	#ascii code for space ' '
	addiu $v0, $0, 11
	syscall			#opcode 11 prints character in a0
	jr $ra
	nop

exit:
	addiu $v0, $0, 10	
	syscall			#opcode 10 ends the program











