#collatz
#start with number > 0
#even? -> next number is half of that
#uneven? -> next number is 3 times plus 1
#equals 1? -> finished

.data
welcome: .asciiz "\Type your number for the collatz loop "

.text
main:
#print introduction text
	la $a0, welcome
	addiu $v0, $0, 4
	syscall			#opcode 4 prints string in a0

#store user input in t0
	addiu $v0, $0, 5
	syscall			#opcode 5 reads integer into v0
	addu $t0, $0, $v0	#move value to t0

#is value even or uneven
	addiu $t2, $0, 2	#will be used to divide by 2
	addiu $t1, $0, 1	#will be used for the comparison at the end
loop:
	div $t0, $t2		#divide by 2, remainder is in hi
	mfhi $t3		#move hi value to t3
	beq $t3, $0, even
	nop
	
#uneven:
	beq $t0, $t1, end	#if t0 = 1 we are finished and go to the end
	nop
	mul $t0, $t0, 3		#else we continue normally
	addiu $t0, $t0, 1
	j print
	nop

even:
	srl $t0, $t0, 1

print:
#print current integer
	addu $a0, $0, $t0
	addiu $v0, $0, 1
	syscall			#opcode 1 prints integer in a0
#print space
	addiu $a0, $0, 0x0020	#ascii code for space ' '
	addiu $v0, $0, 11
	syscall			#opcode 11 prints character in a0
	j loop
	nop
#check if $t0 = 1, that means we stop
	#bne $t0, $t1, loop
	#nop

end:
	addiu $v0, $0, 10	
	syscall			#opcode 10 ends the program
	